module Main exposing (..)

import Html exposing(Html)
import Devanagari exposing(Vowel(..), Consonnant(..), Text(..))
import Pronoun

main : Program Never Model Msg
main =
    Html.beginnerProgram
    { model = ""
    , view = view
    , update = update
    }

type alias Model = String

type Msg
    = NoOp

update : Msg -> Model -> Model
update msg model =
    model

view : Model -> Html Msg
view model =
    Html.div []
        [ Html.h1 [] [ Html.text "Devanagari"]
        , Html.h2 [] [ Html.text "Letters" ]
        , Html.table []
            (Html.tr []
                            (Html.td [] [] :: (
                                List.map
                                (\v -> Html.th [] [viewVowel v])
                                Devanagari.vowels
                                )
                            )
                        ::
                            (List.map
                                (\c -> Html.tr []
                                    (Html.th [] [viewConsonnant c] :: (
                                        List.map
                                        (\v -> Html.td [] [viewText (Devanagari.syllable c v)])
                                        Devanagari.vowels
                                        )
                                        )
                                    )
                                Devanagari.consonnants
                                ))
        , Html.h2 [] [ Html.text "Pronouns" ]
        , Html.ul []
            (List.map
                (\p -> Html.li [] [viewText (Pronoun.text p)])
                Pronoun.pronouns
            )
        ]

viewVowel : Vowel -> Html Msg
viewVowel (VW dev _ lat) =
    viewTransliterated dev lat

viewConsonnant : Consonnant -> Html Msg
viewConsonnant (CS dev lat) =
    viewTransliterated dev lat

viewText : Text -> Html Msg
viewText (TXT dev lat) =
    viewTransliterated dev lat

viewTransliterated : String -> String -> Html Msg
viewTransliterated dev lat =
    Html.div []
        [ Html.p [] [ Html.text dev ]
        , Html.p [] [ Html.text lat ]
        ]
