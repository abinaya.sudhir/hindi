module Devanagari exposing(..)

type Vowel = VW String String String

type Consonnant = CS String String

type Character
    = VWC Vowel
    | CSC Consonnant

type Text = TXT String String

devanagari : Character -> String
devanagari char =
    case char of
        VWC (VW dev _ _) -> dev
        CSC (CS dev _) -> dev

latin : Character -> String
latin char =
    case char of
        VWC (VW _ _ lat) -> lat
        CSC (CS _ lat) -> lat

independant : Vowel -> String
independant (VW ind _ _) =
    ind

dependant : Vowel -> String
dependant (VW _ dep _) =
    dep

syllable : Consonnant -> Vowel -> Text
syllable (CS cdev clat) (VW _ vdep vlat) =
    TXT (cdev ++ vdep) (clat ++ vlat)

vowels : List Vowel
vowels =
    [ VW "अ" "" "a"
    , VW "आ" "ा" "ā"
    , VW "इ" "ि" "i"
    , VW "इ" "ी" "ī"
    , VW "उ" "ु" "u"
    , VW "ऊ" "ू" "ū"
    , VW "ऋ" "ृ" "r̥"
    , VW "ए" "े" "e"
    , VW "ऐ" "ै" "ai"
    , VW "ओ" "ो" "o"
    , VW "औ" "ौ" "au"
    ]

consonnants : List Consonnant
consonnants =
    [ CS "क" "k"
    , CS "ख" "kh"
    , CS "ग" "g"
    , CS "घ" "gh"
    --, CS "ङ" "ng"
    , CS "च" "c"
    , CS "छ" "ch"
    , CS "ज" "j"
    , CS "झ" "jh"
    --, CS "ञ" "ny"
    , CS "ट" "ṭ"
    , CS "ठ" "ṭh"
    , CS "ड" "ḍ"
    , CS "ढ" "ḍh"
    , CS "ण" "ṇ"
    , CS "त" "t"
    , CS "थ" "th"
    , CS "द" "d"
    , CS "ध" "dh"
    , CS "न" "n"
    , CS "प" "p"
    , CS "फ" "ph"
    , CS "ब" "b"
    , CS "भ" "bh"
    , CS "म" "m"
    , CS "य" "y"
    , CS "र" "r"
    , CS "ल" "l"
    , CS "व" "v"
    , CS "श" "ś"
    , CS "ष" "ṣ"
    , CS "स" "s"
    , CS "ह" "h"
    , CS "क़" "q"
    , CS "ख़" "ḵẖ"
    , CS "ग़" "g̱"
    , CS "ज़" "z"
    , CS "ड़" "ṛ"
    , CS "ढ़" "ṛh"
    , CS "फ़" "f"
    ]
