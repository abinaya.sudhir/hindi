module Pronoun exposing(..)

import Devanagari exposing(Text(..))

type Voice
    = Intimate
    | Familiar
    | Formal

type Number
    = Singular
    | Plural

type Distance
    = Nearby
    | Remote

type Pronoun
    = First
    | Second Voice
    | Third Number Distance

pronouns =
    [ First
    , Third Singular Nearby
    , Third Singular Remote
    , Second Intimate
    , Second Familiar
    , Second Formal
    , Third Plural Nearby
    , Third Plural Remote
    ]

text : Pronoun -> Text
text p =
    case p of
        First ->
            TXT "मैं" "maĩ"
        Second Intimate ->
            TXT "तू" "tū"
        Second Familiar ->
            TXT "तुम" "tum"
        Second Formal ->
            TXT "आप" "āp"
        Third Singular Nearby ->
            TXT "यह" "yah"
        Third Singular Remote ->
            TXT "वह" "vah"
        Third Plural Nearby ->
            TXT "ये" "ye"
        Third Plural Remote ->
            TXT "वे" "ve"
